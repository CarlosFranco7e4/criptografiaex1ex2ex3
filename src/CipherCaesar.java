import java.util.Scanner;

public class CipherCaesar {

    public static void main(String[] args) {
        //clau 12
        Scanner sc = new Scanner(System.in);
        String missatgeOriginal = "Hs sqvc rs asbcg qcac hs sqvc rs aog";
        String missatgeResultant = "";
        int clau;
        char lletra;
        System.out.println("Digues la clau: ");
        clau = sc.nextInt();

        for (int i = 0; i < missatgeOriginal.length(); i++){
            lletra = missatgeOriginal.charAt(i);
            if (lletra >= 'a' && lletra <= 'z'){
                lletra = (char)(lletra + clau);
                if (lletra > 'z'){
                    lletra = (char)(lletra - 'z' + 'a' - 1);
                }
                missatgeResultant += lletra;
            }
            else if (lletra >= 'A' && lletra <= 'Z'){
                lletra = (char)(lletra + clau);

                if (lletra > 'Z'){
                    lletra = (char)(lletra - 'Z' + 'A' - 1);
                }

                missatgeResultant += lletra;
            }
            else {
                missatgeResultant += lletra;
            }
        }
        System.out.println("El missatge encriptat és: '" + missatgeResultant + "'");
    }
}
